INTRODUCTION
------------

  * The Taxonomy Manager Lite module provides a panel to re-assign the
    taxonomy(multiple) by another, delete un-assigned taxonomies.

  * This module has two main features:

    * Assign Taxonomy Terms: You can list the nodes with taxonomy term(s) by
      selecting the term(s) from "Current Terms". Depending on whether or not
      you check the "Show nodes with any term instead of all terms" checkbox,
      nodes will be displayed with either both or one of the terms. You can
      then add these terms to the node or replace other terms from
      the "New Terms" drop down.

    * Clean Taxonomy Terms: You can clean the unassigned terms from the
      taxonomy list with a single click.
      In admin/utilities/taxonomy/assign-terms, select the taxonomy(multiple)
      to be replaced. Click on 'Preview' to see the nodes having the taxonomy.
      Select the new taxonomy (multiple) to replace the above one and assign.
      In admin/utilities/taxonomy/delete-empty-terms, remove the unassigned
      taxonomies in a single click.


FEATURES
--------

  * Clean unassigned terms in a vocabulary in one click.

  * Union and intersection operations to list terms.

  * Add or replace operations for listed terms.

  * Preview of node list with taxonomy terms.

  * Can update term name from Preview.

  * Filter terms using all vocabularies in the system.


CONFIGURATION
-------------

  * At admin/utilities/taxonomy/assign-terms, select the taxonomy(multiple)
    to be replaced, click on preview to see the nodes having the taxonomy.
    Select the new taxonomy(multiple) to replace the above one and assign.

  * At admin/utilities/taxonomy/delete-empty-terms remove the unassigned
    taxonomies in a single click.
